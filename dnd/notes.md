# Dragging and dropping

## Dragging

High-level idea for dragging:

1. Take a mousedown stream
1. Map it to a mousemove stream
1. Map to mouse coordinates
1. Scan/reduce and later map the stream of coordinates to get a delta
while keeping track of the origin

```
// We need to listen to mousemove on the document because when we start
// to drag, we'll stop emitting mousemoves on the button itself!
// And using body isn't necessarily good enough because it only expands
// to contain all the content. So if you just have a button, the body
// height will not be very high. Document will always be the whole page.
const mouseMove$ = Observable.fromEvent(document, 'mousemove');

// This *could* be on the button, but to be safe and to make sure that
// we end the stream, we listen on the document.
// --> This is a general pattern: start by listening to something
//       specific, then listen more widely to make sure we finish.
const mouseUp$ = Observable.fromEvent(document, 'mouseup');

const dragDelta$ = Observable.fromEvent(button, 'mousedown')
  .switchMap(e => {
    const originX = e.clientX;
    const originY = e.clientY;
    return mouseMove$.map(e => {
      const x = e.clientX;
      const y = e.clientY;
      return {
        dx: x - originX,
        dy: y - originY,
        type: 'DRAG_MOVE'
      };
    }).takeUntil(mouseUp$)
    .concat(Observable.of({ type: 'DRAG_END' }));
  });
```

### What is considered dragging?

Two definitions:

```
// This stream is dragging as soon as the mouse is down
const isDragging1$ = Observable.fromEvent(button, 'mousedown')
    .map(e => true)
    .merge(
        Observable.fromEvent(button, 'mouseup')
            .map(e => false)
    );

// This stream is only dragging if we move the mouse after pressing down
const isDragging2$ = Observable.fromEvent(button, 'mousedown')
    .switchMap(e => mouseMove$
        .take(1)
        .map(e => true)
        .merge(Observable.never())
        .takeUntil(mouseUp$)
        .concat(Observable.of(false))
    );
```

### Calculating the position of the button

With dx and dy

```
function calculateButtonPosition(dx, dy) {
  const buttonPosition = button.getBoundingClientRect();
  console.log(buttonPosition);
  return {
    left: buttonPosition.left + dx,
    right: buttonPosition.right + dx,
    top: buttonPosition.top + dy,
    bottom: buttonPosition.bottom + dy
  }
}
```

Without dx and dy

```
function calculateButtonPosition(dx, dy) {
  return button.getBoundingClientRect();
}
```

## Dropping

### Target detection

```
const isHovering$ = Observable.fromEvent(box, 'mouseenter')
    .map(e => true)
    .merge(
        Observable.fromEvent(box, 'mouseleave')
            .map(e => false)
    );
```

```
// Outline of how to determine if button is over box:
// - the origins of button and box
// - width/height of button and box
// - start and end X and Y of box
// - take dx and dy of button drag, calculate its absolute x and y and
//   compare that with X and Y of box
// - emit true/false, and then use this instead of isHovering$
const isButtonOverBox$ = dragDelta$.map(({ dx, dy }) => {
  const buttonPosition = button.getBoundingClientRect();
  const boxPosition = box.getBoundingClientRect();
  return isInBox(buttonPosition, boxPosition);
});

isButtonOverBox$.subscribe(isButtonOverBox => {
    if (isButtonOverBox) {
      box.style.backgroundColor = '#000';
    } else {
      box.style.backgroundColor = null;
    }
});
```

```
function isInBox(buttonPosition, boxPosition) {
    return buttonPosition.left > boxPosition.left &&
        buttonPosition.right < boxPosition.right &&
        buttonPosition.top > boxPosition.top &&
        buttonPosition.bottom < boxPosition.bottom;
}
const isButtonOverBox = (buttonId) => {
    const buttonPosition = buttons[buttonId].element.getBoundingClientRect();
    const boxPosition = box.getBoundingClientRect();
    return isInBox(buttonPosition, boxPosition);
};
```

### DOM manipulation

```
dragDelta$.subscribe(({ type }) => {
  if (type === 'DRAG_MOVE' && isButtonOverBox()) {
    box.style.backgroundColor = '#000';
  } else {
    box.style.backgroundColor = null;
  }
});

// The following two statements are equivalent
dragDelta$
  .filter(dragDelta => dragDelta.type === 'DRAG_END')
  .filter(isButtonOverBox)
  .subscribe(() => box.appendChild(button));

dragDelta$
  .withLatestFrom(isButtonOverBox$, (dragDelta, isButtonOverBox) => isButtonOverBox)
  .filter(x => x)
  .subscribe((dragDelta, isButtonOverBox) => {
      if (isButtonOverBox) {
        box.appendChild(button);
      }
  });
```

#### Gotcha!

```
// Doesn't work because mouseenter/leave -- while we're dragging the
// mouse, we're not actually entering/leaving the box so the colour
// doesn't change. It only changes briefly if we move the mouse very
// suddenly and for a large distance, because when we do that, there is
// a brief instant where the mouse is temporarily over the box when the
// button hasn't quite caught up to the mouse yet.
isDragging2$
  .combineLatest(isHovering$, (isDragging, isHovering) => isDragging && isHovering)
  .subscribe(isDraggingOverBox => {
    if (isDraggingOverBox) {
      box.style.backgroundColor = '#000';
    } else {
      box.style.backgroundColor = null;
    }
  });
```

## Putting it together -- dragging and dropping arbitrarily in a box

```
getButtonDrag$(button) // this is just the dragDelta$ from above, where
                       // button is the DOM element
  .subscribe(({ dx, dy, type }) => {
    const buttonPosition = button.getBoundingClientRect();
    const state = {
      buttonPosition,
      isButtonOverBox: isButtonOverBox(),
      shouldSnap: shouldSnap(snapPoint)(buttonPosition),
    };
    // The function that is passed to requestAnimationFrame will be
    // run and then the browser will redraw. Usually you would
    // manipulate the DOM so that the browser can redraw everything at
    // once, instead of doing DOM manipulations in random places and
    // basically forcing browser to re-redraw after each individual DOM
    // manipulation.
    requestAnimationFrame(() => {
      console.log(state);
      if (type === 'DRAG_MOVE' && state.isButtonOverBox) {
        box.style.backgroundColor = '#000';
      } else {
        box.style.backgroundColor = null;
      }
      if (type === 'DRAG_END') {
        if (state.isButtonOverBox) {
          // Initially we did DOM manipulation but later we settled on
          // simply changing the position to be absolute (for placement
          // onto the box) or static (to reset it back to its normal
          // position in the DOM)
          // box.appendChild(button);
          button.style.position = 'absolute';
          if (state.shouldSnap) {
            button.style.left = `${snapPoint.left}px`;
            button.style.top = `${snapPoint.top}px`;
          } else {
            button.style.left = `${state.buttonPosition.left}px`;
            button.style.top = `${state.buttonPosition.top}px`;
          }
        } else {
          button.style.position = 'static';
        }
        button.style.transform = '';
      } else {
        button.style.transform = `translate(${dx}px, ${dy}px)`;
      }
    });
  });
```