import {Observable} from "rxjs";

// TODO 1: circular dependency from initial state to current game state -
// we should be able to signal the start of a new game when the old game
// finishes
// TODO 2: split things up into multiple files
// TODO 3: randomise the start positions of the buttons
// TODO 4: state shape refactor - right now we have calculated button position
// in public but all the button stuff in static. Maybe we want to get rid of
// static and have everything in public, so let's see if we can merge the
// streams into the state differently.
// TODO 5: stop hardcoding the height and width of the pieces
// TODO *: decouple event source and state - rendering HTML based on state,
// not creating button elements initially and having that feed into state -
// this means that we don't change our stream based on listening to DOM
// events, but have some starting state that's independent of the DOM and use
// that to derive the DOM

const shouldSnap = (snapPoint) => (buttonPosition) => {
    const snapTolerance = 50;
    return Math.abs(snapPoint.top - buttonPosition.top) <= snapTolerance &&
        Math.abs(snapPoint.left - buttonPosition.left) <= snapTolerance;
};

const mouseMove$ = Observable.fromEvent(document, 'mousemove');
const mouseUp$ = Observable.fromEvent(document, 'mouseup');

// height and width are hardcoded for now, but later we'd want to
// calculate them based on the height/width of the total grid and
// the number of pieces passed in (which would be an indication of
// difficulty
const pieceHeight = 60;
const pieceWidth = 60;
const numPieces = 25;

const makeInitialState = (height, width, numPieces) => {
    const buttonIds = [];
    for (let i = 0; i < numPieces; i++) { // ugh!
        buttonIds.push(`button${i}`);
    }

    const buttonCharacteristics = buttonIds.map((buttonId, index) => {
        const element = document.createElement('button');
        document.getElementById('container').appendChild(element);
        element.innerText = index;
        element.style.height = `${pieceHeight}px`;
        element.style.width = `${pieceWidth}px`;
        const position = element.getBoundingClientRect();

        return {
            element,
            snapPoint: {
                top: Math.floor((index * pieceWidth) / width) * pieceHeight,
                left: (index * pieceWidth) % width
            },
            position
        }
    });

    const buttons = buttonCharacteristics.reduce((acc, value, index) => {
        return {
            ...acc,
            [buttonIds[index]]: value
        }
    }, {});

    return {
        public: {},
        private: {},
        static: {
            buttons
        }
    };
};

const initialState = makeInitialState(300, 300, numPieces);

const getButtonsSelector = state => state.static.buttons;
const getButtonWithId = state => buttonId => getButtonsSelector(state)[buttonId];
const getElementWithButtonId = state => buttonId => getButtonWithId(state)(buttonId).element;
const getSnapPointForButtonId = state => buttonId => getButtonWithId(state)(buttonId).snapPoint;
const getPositionForButtonId = state => buttonId =>
    state.public[buttonId] && state.public[buttonId].buttonPosition || getButtonWithId(state)(buttonId).position;

const buildGameState$ = state => {
    const buttonDragStreams = Object.keys(getButtonsSelector(state)).map(getButtonDrag$(state));

    const buttonDragPosition$ = Observable.merge(...buttonDragStreams)
        .filter(value => value.type === 'DRAG_MOVE')
        .map(value => {
            return {
                [value.id]: {
                    buttonPosition: getButtonPosition(value.buttonPosition, {
                        dx: value.dx,
                        dy: value.dy
                    })
                }
            };
        });

    const buttonSnapPosition$ = Observable.merge(...buttonDragStreams)
        .filter(value => value.type === 'DRAG_END')
        .map(value => {
            return {
                [value.id]: {
                    buttonPosition: getButtonPositionWithSnap(state)(value.id, value.buttonPosition, {
                        dx: value.dx,
                        dy: value.dy
                    })
                }
            }
        });

    const buttonPosition$ = Observable.merge(buttonDragPosition$, buttonSnapPosition$)
        .scan((acc, next) => ({...acc, ...next}), {})
        .startWith({});

    const currentlyDraggingButton$ = Observable.merge(...buttonDragStreams)
        .filter(({type}) => type !== 'DRAG_MOVE')
        .map(({type, id}) => {
            if (type === 'DRAG_START') {
                return {draggingButton: id};
            }
            if (type === 'DRAG_END') {
                return {draggingButton: null};
            }
        })
        .startWith({draggingButton: null});

    return Observable.combineLatest(
        buttonPosition$,
        currentlyDraggingButton$,
        (buttonPositions, currentlyDraggingButton) => ({
            ...state,
            public: buttonPositions,
            private: currentlyDraggingButton,
        })
    );
};

const getButtonDrag$ = state => buttonId => Observable.fromEvent(getElementWithButtonId(state)(buttonId), 'mousedown')
    .switchMap(e => {
        const originX = e.clientX;
        const originY = e.clientY;
        const buttonPosition = getElementWithButtonId(state)(buttonId).getBoundingClientRect();
        return mouseMove$
            .map(e => {
                const x = e.clientX;
                const y = e.clientY;
                return {
                    dx: x - originX,
                    dy: y - originY,
                    type: 'DRAG_MOVE',
                    id: buttonId,
                    buttonPosition
                };
            })
            .takeUntil(mouseUp$)
            .concat(Observable.create(observer => {
                const currentButtonPosition = getElementWithButtonId(state)(buttonId).getBoundingClientRect();
                observer.next({
                    dx: currentButtonPosition.left - buttonPosition.left,
                    dy: currentButtonPosition.top - buttonPosition.top,
                    type: 'DRAG_END',
                    id: buttonId,
                    buttonPosition
                });
                observer.complete();
            }))
            .startWith({
                type: 'DRAG_START',
                id: buttonId
            });
    });

const getButtonPosition = (buttonOrigin, {dx, dy}) => {
    return {
        top: buttonOrigin.top + dy,
        left: buttonOrigin.left + dx
    };
};

const getButtonPositionWithSnap = state => (buttonId, buttonOrigin, {dx, dy}) => {
    const snapPointForButton = getSnapPointForButtonId(state)(buttonId);
    const buttonPosition = getButtonPosition(buttonOrigin, {dx, dy});
    return shouldSnap(snapPointForButton)(buttonPosition) ? snapPointForButton : buttonPosition;
};

buildGameState$(initialState).subscribe(state => {
    requestAnimationFrame(() => {
        Object.keys(getButtonsSelector(state)).forEach(renderButton(state));
    });
});

const renderButton = (state) => (buttonId) => {
    const button = getElementWithButtonId(state)(buttonId);
    const buttonPosition = getPositionForButtonId(state)(buttonId);

    button.style.position = 'absolute';
    button.style.left = `${buttonPosition.left}px`;
    button.style.top = `${buttonPosition.top}px`;
};

