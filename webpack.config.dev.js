var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: [
        './dnd/index'
    ],
    devtool: 'eval-cheap-module-source-map',
    output: {
        path: path.resolve(__dirname, 'dnd'),
        filename: 'bundle.js',
        publicPath: '/dnd'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true
                },
                exclude: /node_modules/
            }
        ]
    }
};
